package fr.dow.gamedata;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;


public class RelicChunk implements Cloneable {
	
	// Header Data
	protected String  _type="";
	protected String  _id="";
	protected Integer _version=0;
	protected Integer _datasize=0;
	protected String  _name="";
	protected Integer _headersize=20;
	
	private static final byte[] zeroCharByte={0};
	public static final String zeroChar=new String(zeroCharByte);
	
	public Object clone() {
		Object o = null;
		try {
			o = super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}
		return o;
	}
	
	// Functions used to read inside the file
	
	protected static void readFully(InputStream streamIn, byte[] array ) throws IOException {
		int bytesread = 0;
		int i;
		while (bytesread < array.length) {
			// if (bytesread>0) System.out.println(array.length - bytesread + " bytes still needed for a total of "+array.length);
			i = streamIn.read(array, bytesread, array.length - bytesread);
			if (i < 0) throw new IOException("Ran out of bytes to read!");
			bytesread += i;
		}
	}
	
	static private String readText(InputStream streamIn, Integer size ) throws IOException {
		byte[] bytedata=new byte[size];
		readFully(streamIn, bytedata);
		return new String(bytedata);
	}
	
	static private int readInteger(InputStream streamIn ) throws IOException {
		byte[] bytedata=new byte[4];
		readFully(streamIn, bytedata);
		return bytedata[3]<<24 | (bytedata[2]&0xff)<<16 | (bytedata[1]&0xff)<<8 | (bytedata[0]&0xff);
	}
	
	static protected void writeInteger(OutputStream outputStream, int intOut) throws IOException {
		outputStream.write( (intOut & 0x000000FF) >> 0 );
		outputStream.write( (intOut & 0x0000FF00) >> 8 );
		outputStream.write( (intOut & 0x00FF0000) >> 16 );
		outputStream.write( (intOut & 0xFF000000) >> 24 );
	}
	
	// Empty Chunk
	protected RelicChunk(String type, String id, int version, String name) {
		_type=type;
		_id=id;
		_version=version;
		_name=name;
	}
	
	protected RelicChunk(RelicChunk chunk) {
		_type=chunk._type;
		_id=chunk._id;
		_version=chunk._version;
		_datasize=chunk._datasize;
		_name=chunk._name;
		_headersize=chunk._headersize;
	}
	
	// Constructor based on loading from a file
	protected RelicChunk(InputStream streamIn) throws IOException {
		
		_type=readText(streamIn,4);
		_id=readText(streamIn,4);
		_version=readInteger(streamIn);
		_datasize=readInteger(streamIn);
		
		int namesize=readInteger(streamIn);
		_name=readText(streamIn,namesize);
		
		_headersize=20+namesize;
			
	}
	
	public static RelicChunk loadChunk(InputStream streamIn) throws IOException {
	
		RelicChunk newChunk=new RelicChunk(streamIn);
		
		if (RelicChunkFOLD.type.contentEquals(newChunk.get_type())) {
			return new RelicChunkFOLD(newChunk,streamIn);
		}
		else if (RelicChunkDATA.type.contentEquals(newChunk.get_type())) {
			return new RelicChunkDATA(newChunk,streamIn);
		}
		
		throw new IOException("Unknown type in Chunk : " + newChunk.get_type());
		
	}
	
	public void write(OutputStream outputStream) throws IOException {
		outputStream.write(_type.getBytes());
		outputStream.write(_id.getBytes());
		writeInteger(outputStream, _version);
		writeInteger(outputStream, _datasize);
		writeInteger(outputStream, _name.length());
		outputStream.write(_name.getBytes());
		
		dataWrite(outputStream);
		
	}
	
	public void dataWrite(OutputStream outputStream)  throws IOException {
		throw new RuntimeException("unhandled writeData in RelicChunk" );
	}
		
	public int updateSize() {
		throw new RuntimeException("unhandled updateSize in RelicChunk" );
	}
		
	// Custom Getters
	public Integer get_size() {
		return _datasize+_headersize;
	}
	
	// Standard Getters
	
	public String get_type() {
		return _type;
	}

	public String get_id() {
		return _id;
	}

	public Integer get_version() {
		return _version;
	}

	public Integer get_datasize() {
		return _datasize;
	}

	public String get_name() {
		return _name;
	}

	public Integer get_headersize() {
		return _headersize;
	}
	
	public void set_id(String ID) {
		_id=ID;
	}
	
	public void set_name(String name) {
		_name=name;
	}
	
	public void set_version(Integer version) {
		_version=version;
	}
}
