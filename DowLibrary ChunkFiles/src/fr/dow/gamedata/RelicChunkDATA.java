package fr.dow.gamedata;

import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public class RelicChunkDATA extends RelicChunk {
	
	public static final String type="DATA";
	
	private ByteBuffer _data;
	
	//
	// Constructors
	//
	
	public RelicChunkDATA(String id, int version, String name) {
		super(type,id,version,name);
	}
	
	public RelicChunkDATA(RelicChunk chunk, InputStream streamIn) throws IOException {
		
		super(chunk);
		
		_data=ByteBuffer.allocate(_datasize).order(ByteOrder.LITTLE_ENDIAN);
		readFully(streamIn, _data.array());
	}
	
	//
	// Utilities
	//
	
	public Object clone() {
		
		RelicChunkDATA o = (RelicChunkDATA)super.clone();
		
		o._data=ByteBuffer.wrap(_data.array().clone()).order(ByteOrder.LITTLE_ENDIAN);
		o._data.position(_data.position());
		
		return o;
		
	}
	
	public void dataWrite(OutputStream outputStream)  throws IOException {
		outputStream.write(_data.array());
	}
	
	public int updateSize() {
		_headersize=20+_name.length();
		_datasize=_data.capacity();
		return _headersize+_datasize;
	}
	
	//
	// write data
	//
	
	public void dataWrapArray(byte[] data) {
		_data=ByteBuffer.wrap(data).order(ByteOrder.LITTLE_ENDIAN);
	}
	
	//
	// Getters
	//
	
	public byte[] dataArray() {
		return _data.array();
	}

	public byte[] dataSubArray(Integer Pos, Integer length) {
		_data.position(Pos);
		return dataSubArray(length);
	}

	//
	// Pointer access
	//
	
	public int dataPosition(){
		return _data.position();
	}
	
	public void dataPosition(int ptr){
		_data.position(ptr);
	}
	
	public void dataSkipBytes(int byteNbr){
		_data.position(_data.position()+byteNbr);
	}
	
	//
	// Read data using pointer
	//
	
	public byte[] dataSubArray(Integer length) {
		byte[] tmp=new byte[length];
		_data.get(tmp);
		return tmp;
	}
		
	public String dataGetDblString() {
		int ncar=_data.getInt();
		String newString="";
		for (int i=0;i<ncar;i++) newString=newString+_data.getChar();
		return newString;
	}
	
	public String dataGetString() {
		return dataGetString(_data.getInt());		
	}
	
	public String dataGetString(Integer length) {
		return new String(dataSubArray(length));		
	}
	
	public String dataGetString(Integer Pos, Integer length) {
		_data.position(Pos);
		return dataGetString(length);
	}
	
	public Integer dataGetInt(Integer Pos){
		return _data.getInt(Pos);		
	}
	
	public Integer dataGetInt(){
		return _data.getInt();		
	}
	
	public Integer dataGetShort(Integer Pos){
	    return (int) _data.getShort(Pos);		
	}
	
	public Integer dataGetShort(){
		return (int) _data.getShort();		
	}
	
	public Integer dataGetByte(){
		return 0xff & _data.get();				
	}

	public Float dataGetFloat(Integer Pos){
		return _data.getFloat(Pos);		
	}
	
	public Float dataGetFloat(){
		return _data.getFloat();				
	}
	
	//
	// Write data using pointer
	//
		
	public void dataPutFloat(Float value) {
		_data.putFloat(value);
	}

	public void dataPutFloat(int index,Float value) {
		_data.putFloat(index, value);
	}
	
	public void dataPutInt(int value) {
		_data.putInt(value);
	}

	public void dataPutInt(int index,int value) {
		_data.putInt(index, value);
	}
	
}
