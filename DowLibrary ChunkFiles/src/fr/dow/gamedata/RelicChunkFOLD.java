package fr.dow.gamedata;

import java.io.OutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Iterator;
import java.util.Vector;

public class RelicChunkFOLD extends RelicChunk {

	public static final String type="FOLD";
	
	private Vector<RelicChunk> _subChunks=new Vector<RelicChunk>();
	
	public RelicChunkFOLD(String id, int version, String name) {
		super(type,id,version,name);
	}
	
	public RelicChunkFOLD(RelicChunk chunk, InputStream streamIn) throws IOException {
		
		super(chunk);
		
		int loadeddata=0;
		
		while (loadeddata<_datasize) {
		
			RelicChunk NewSubChunk=RelicChunk.loadChunk(streamIn);
			
			loadeddata+=NewSubChunk.get_size();

			_subChunks.add(NewSubChunk);
			
		}
		
	}
	
	@SuppressWarnings("unchecked") //casting cannot be checked when cloning vector
	public Object clone() {
		
		RelicChunkFOLD o = (RelicChunkFOLD)super.clone();
		
		o._subChunks=(Vector<RelicChunk>)o._subChunks.clone();
		
		for (int i=0;i<_subChunks.size();i++) o._subChunks.set(i, (RelicChunk) o._subChunks.get(i).clone());
		
		return o;
		
	}
	
	public void dataWrite(OutputStream outputStream)  throws IOException {
		for (int i=0;i<_subChunks.size();i++) _subChunks.get(i).write(outputStream);
	}
		
	public int updateSize() {
		_headersize=20+_name.length();
		_datasize=0;
		for (int i=0;i<_subChunks.size();i++) _datasize+=_subChunks.get(i).updateSize();
		return _datasize+_headersize;
	}
	
	public Vector<RelicChunk> get_subChunks() {
		return _subChunks;
	}
	
	public Vector<RelicChunk> subChunkList(String searchType) {
		
		Vector<RelicChunk> newVec= new Vector<RelicChunk>();
		
		Iterator<RelicChunk> it=_subChunks.iterator();
		
		while (it.hasNext()) {
			RelicChunk tmpchunk=it.next();
			if ( tmpchunk.get_id().contentEquals(searchType) ) 
				newVec.add(tmpchunk);
		}
		
		return newVec;
	}
	
	private RelicChunk subChunk(String searchType) {
		
		RelicChunk tmpchunk = null;
		
		Iterator<RelicChunk> it=_subChunks.iterator();
		
		while (it.hasNext()) {
			tmpchunk=it.next();
			if ( tmpchunk.get_id().contentEquals(searchType) ) return tmpchunk;
			
		}
		
		return null;
		
	}
	
	public RelicChunkFOLD subChunkFOLD(String searchType) {
		return (RelicChunkFOLD) subChunk(searchType);
	}
	
	public RelicChunkDATA subChunkDATA(String searchType) {
		return (RelicChunkDATA) subChunk(searchType);
	}
	
}
