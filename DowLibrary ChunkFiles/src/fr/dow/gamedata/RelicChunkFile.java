package fr.dow.gamedata;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.Vector;

import fr.dow.gamedata.filesystem.DowFileVersion;

public class RelicChunkFile extends Vector<RelicChunk> implements Serializable  {

	public final static byte[] FILEHEADER={ 
		0x52, 0x65, 0x6c, 0x69, 
		0x63, 0x20, 0x43, 0x68, 
		0x75, 0x6e, 0x6b, 0x79, 
		0x0d, 0x0a, 0x1a, 0x00,
		0x01, 0x00, 0x00, 0x00,
		0x01, 0x00, 0x00, 0x00
	};
	
	public RelicChunkFile() {
		super();
	}
	
	public RelicChunkFile(InputStream streamIn) throws IOException {
			
		this();
		
		readObject(streamIn);
		
		streamIn.close();
		
	}

	public RelicChunkFile(DowFileVersion dowFileVersion) throws IOException {
		this(new BufferedInputStream(new ByteArrayInputStream(dowFileVersion.getData())));
	}
	
	public RelicChunkFile(File file) throws IOException {
		this(new BufferedInputStream(new FileInputStream(file)));
	}	

	// TODO remove String access which is not correct.
	public RelicChunkFile(String filename) throws IOException {
		this(new BufferedInputStream(new FileInputStream(filename)));
	}	

	public void save(File fileOut) throws IOException {
		FileOutputStream outputStream=new FileOutputStream(fileOut);
		writeObject(outputStream);
		outputStream.close();
	}
	
    // m�thode readObject, utilis�e pour reconstituer un objet s�rializ�
   public void readObject(InputStream streamIn) throws IOException {

	   // vide la table
	   this.clear();
	   
	   //skip first 24 bytes
		streamIn.skip(24);
		
		while (streamIn.available()>0) {
			RelicChunk NewSubChunk=RelicChunk.loadChunk(streamIn);
			this.add(NewSubChunk);
		}

   }

    // m�thode writeObject, utilis�e lors de la s�rialization
   public void writeObject(OutputStream outputStream) throws IOException {
		outputStream.write(FILEHEADER);
		for (int i=0;i<size();i++) get(i).updateSize();
		for (int i=0;i<size();i++) get(i).write(outputStream);
  }
	
}
