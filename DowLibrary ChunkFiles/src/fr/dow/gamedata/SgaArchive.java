package fr.dow.gamedata;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.HashMap;
import java.util.zip.DataFormatException;
import java.util.zip.Inflater;

/*
===== SGA File Format =====

	0x000000 - Identifier ASCII String (8)
	0x000008 - Version (4)
	0x00000C - Unknown (4)
	0x000010 - Unknown (4)
	0x000014 - Unknown (4)
	0x000018 - Unknown (4)
	0x00001C - Archive Type UNICODE String (128)
	0x00009C - Majorly Unknown (16)
	0x0000AC - Data Header Size (4) - Size of the header block starting from B4
	0x0000B0 - Data Offest (4) - Offset from base (include base offset = B4)
	
	0x0000B4 - Header block (octet 180)
		0x0000B4 - TOC Offset (4) (Offset from B4)
		0x0000B8 - TOC Count (2)
		0x0000BA - Dir Offset (4) (Offset from B4) - Points to an array of Directory Info
		0x0000BE - Dir Count (2)
		0x0000C0 - File Offset (4) (Offset from B4) - Points to an array of File Info
		0x0000C4 - File Count (2)
		0x0000C6 - Item Offset (4) (Offset from B4)
		0x0000CA - Item Count (2)
		
		TOC Block - (start at global 0x0000CC)
			0x0000CC - TOC Alias ASCII String (64) (Addr: B4 + TOC Offset)
			0x00010C - TOC Start Name ASCII String (64)
			0x00014C - TOC Start Dir (2)
			0x00014E - TOC End Dir (2)
			0x000150 - TOC Start File (2)
			0x000152 - TOC End File (2)
			0x000154 - TOC Folder Offset (4)
		
		Directory Block
			+ 0x000 - Name Offset (4) (+180+ItemOffset)
			+ 0x004 - Sub Dir ID Begin (2)
			+ 0x006 - Sub Dir ID End (2) (Begin->End-1)
			+ 0x008 - File ID Begin (2)
			+ 0x00A - File ID End (2) (Begin+1->End)
		
		File Block
			+ 0x000 - Name Offset (4) (+181+ItemOffset)
			+ 0x004 - Flags < 0 = uncompressed, 16 = zlib large file, 32 = zlib small file (< 4kb)
			+ 0x008 - Data Offset (4) (+ B0 Data Offset)
			+ 0x00C - Unknown (4) => decompressed file size
			+ 0x010 - Data Len (4)
	
	
*/
public class SgaArchive {

	private static HashMap<File,SgaArchive> loadedSgaArchive=new HashMap<File,SgaArchive>();

	private static String rootChunkName;
	private static String internalFileName;
	
	private File sgaFile;
	
	private HashMap<String, Integer> sgaLinkedDirList;
	
	private int dataOffset;
	
	private SgaDir[] sgaDirList;
	private SgaFile[] sgaFileList;
	
	// ================================================================================================
	//
	// Constructor
	//
	// ================================================================================================
	
	@SuppressWarnings("unused")
	public static SgaArchive getSgaArchive(File sgaFile) throws IOException {
		
		if (loadedSgaArchive.containsKey(sgaFile)) return loadedSgaArchive.get(sgaFile);
		
		SgaArchive newSgaArchive=new SgaArchive();
		
		newSgaArchive.sgaFile=sgaFile;
		
		FileInputStream sgaStream=new FileInputStream(sgaFile);
		
		sgaStream.skip(172);
		
		// read 1st block of data to get header size
		ByteBuffer startData=ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN);
		readFully(sgaStream, startData.array());
		int headerSize=startData.getInt();
		newSgaArchive.dataOffset=startData.getInt();

		// read header data
		ByteBuffer headerData=ByteBuffer.allocate(headerSize).order(ByteOrder.LITTLE_ENDIAN);
		readFully(sgaStream, headerData.array());
		sgaStream.close();
		
		// load header
		int tocOffset=headerData.getInt();
		int tocCount=headerData.getShort();
		int dirOffset=headerData.getInt();
		int dirCount=headerData.getShort();
		int fileOffset=headerData.getInt();
		int fileCount=headerData.getShort();
		int itemOffset=headerData.getInt();
		int itemCount=headerData.getShort();
		
		// read TOC block
		headerData.position(tocOffset);
		rootChunkName = readName(headerData, tocOffset);
		internalFileName = readName(headerData, tocOffset+64);
		
		headerData.position(tocOffset+128);
		int tocStartDir = headerData.getShort();
		int tocEnDir = headerData.getShort();
		int tocStartFile = headerData.getShort();
		int tocEndFile = headerData.getShort();
		int tocFolderOffset = headerData.getInt();
		
		// read dir blocks
		headerData.position(dirOffset);
		newSgaArchive.sgaDirList=new SgaDir[dirCount];
		newSgaArchive.sgaLinkedDirList=new HashMap<String, Integer>();
		for (int i=0;i<dirCount;i++) {
			newSgaArchive.sgaDirList[i]=new SgaDir(headerData,itemOffset);
			newSgaArchive.sgaLinkedDirList.put(newSgaArchive.sgaDirList[i].dowPath, i);
		}		
		
		// read file blocks
		headerData.position(fileOffset);
		newSgaArchive.sgaFileList=new SgaFile[fileCount];
		for (int i=0;i<fileCount;i++) newSgaArchive.sgaFileList[i]=new SgaFile(headerData,itemOffset);
		
		
		// save sgaArchive for future use
		loadedSgaArchive.put(sgaFile, newSgaArchive);
		
		return newSgaArchive;
		
	}

	
	// ================================================================================================
	//
	// Getters
	//
	// ================================================================================================

	// sgaFile

	public File get_SgaFile() {
		return sgaFile;
	}

	// internal sga datas

	public String getRootChunkName() {
		return rootChunkName;
	}

	public String getInternalFileName() {
		return internalFileName;
	}

	// Directories
	
	public int get_topDirID() {
		return 0;
	}

	public String get_dirName(int dirID) {
		return sgaDirList[dirID].name;
	}

	public String get_dirDowPath(int dirID) {
		return sgaDirList[dirID].dowPath;
	}

	public int get_subDirIDBegin(int dirID) {
		return sgaDirList[dirID].beginSubDirID;
	}

	public int get_subDirIDEnd(int dirID) {
		return sgaDirList[dirID].endSubDirID;
	}

	public int get_subFileIDBegin(int dirID) {
		return sgaDirList[dirID].beginSubFileID;
	}

	public int get_subFileIDEnd(int dirID) {
		return sgaDirList[dirID].endSubFileID;
	}

	// Files
	
	public String get_fileName(int fileID) {
		return sgaFileList[fileID].name;
	}

	private int get_fileFlags(int fileID) {
		return sgaFileList[fileID].flags;
	}

	private int get_fileDataOffset(int fileID) {
		return sgaFileList[fileID].offset;
	}

	private int get_fileDataSize(int fileID) {
		return sgaFileList[fileID].size;
	}

	public int get_fileFinalSize(int fileID) {
		return sgaFileList[fileID].finalSize;
	}
	
	// ================================================================================================
	//
	// Access to files inside the sga Archive
	//
	// ================================================================================================
	
	// return file content as byte[]
	public byte[] getFile(Integer filePtr) throws IOException  {

		if (filePtr==null) return null;
		
		int fileDataOffset=get_fileDataOffset(filePtr);
		int fileDataSize=get_fileDataSize(filePtr);
		int fileFinalSize=get_fileFinalSize(filePtr);
		int fileFlags=get_fileFlags(filePtr);
		
		// recover data
		RandomAccessFile file=new RandomAccessFile(sgaFile, "r");
		file.seek(dataOffset+fileDataOffset);
		byte[] filebytes=new byte[fileDataSize];
		file.readFully(filebytes);
		file.close();
		
		// return data if not compressed
		if (fileFlags==0) return filebytes;
		
		
		// start decompresser
		Inflater decompresser = new Inflater();
		decompresser.setInput(filebytes);
		
		// decompress
		byte[] outfile=new byte[fileFinalSize];
		
		try {
			decompresser.inflate(outfile);
		} catch (DataFormatException e) {
			e.printStackTrace();
			throw new IOException("DataFormatException for Ptr " + filePtr + " in " + sgaFile.getPath());
		}

		//return decompressed file
		return outfile;
		
	}
	
	// root method, should be useless now
	public Integer getFilePtr(String filename, Integer fileDirID) {
		
		int filePtrEnd=get_subFileIDEnd(fileDirID);
		for (int filePtr=get_subFileIDBegin(fileDirID); filePtr<filePtrEnd; filePtr++ )
			if (get_fileName(filePtr).equalsIgnoreCase(filename)) return filePtr;
		
		return null;
	}
  
	// root method, should be useless now
	public Integer getFilePtr(String searchFileName){

		// format search name
		String fullfilename = searchFileName.toLowerCase().replace('\\','/');
		
		// get directory if it exists
		Integer fileDirID;
		if (fullfilename.lastIndexOf('/')>0) 
			fileDirID=sgaLinkedDirList.get(fullfilename.substring(0,fullfilename.lastIndexOf('/')));
		else
			fileDirID=sgaLinkedDirList.get("");
		
		if (fileDirID==null) return null; 
		
		// search file in directory
		String filename=fullfilename.substring(fullfilename.lastIndexOf('/')+1);

		return getFilePtr(filename,fileDirID);
	}
	
	// root method, should be useless now
	public byte[] getFile(String fullfilename) throws IOException  {
		return getFile(getFilePtr(fullfilename));
	}
		
	// ================================================================================================
	//
	// private objects for file management, used to store Data
	//
	// ================================================================================================
	
	private static class SgaFile {
		String name;
		int flags;
		int offset;
		int size;
		int finalSize;
		
		public SgaFile(ByteBuffer header, int itemOffset) {
			name=readName(header, itemOffset+header.getInt()).replace('\\','/').toLowerCase();
			flags=header.getInt();
			offset=header.getInt();
			size=header.getInt();
			finalSize=header.getInt();
		}
	}
	
	private static class SgaDir {
		String name;
		String dowPath;
		int beginSubDirID;
		int endSubDirID;
		int beginSubFileID;
		int endSubFileID;
		
		public SgaDir(ByteBuffer header, int itemOffset) {
			dowPath=readName(header, itemOffset+header.getInt()).replace('\\','/').toLowerCase();
			name=dowPath.substring(dowPath.lastIndexOf('/')+1);
			beginSubDirID=header.getShort();
			endSubDirID=header.getShort();
			beginSubFileID=header.getShort();
			endSubFileID=header.getShort();
		}
	}

	// ================================================================================================
	//
	// Utilities
	//
	// ================================================================================================
	
	/** Reads a string from buffer starting at pos
	 * 
	 * @param buffer
	 * @param pos
	 * @return
	 */
	private static String readName(ByteBuffer buffer, int pos) {
		
		buffer.mark();
		
		buffer.position(pos);
		int size=0; while (buffer.get()!=0) {size++;}
		byte[] stringBytes=new byte[size];
		
		buffer.position(pos);
		buffer.get(stringBytes);
		
		buffer.reset();
		
		return new String(stringBytes);
	}
	
	/** Read streamIn and stores bytes in array
	 * 
	 * @param streamIn : source
	 * @param array : data to read, size is used to define how many bytes should be read (streamIn has to be big enough).
	 * @throws IOException
	 */
	private static void readFully(InputStream streamIn, byte[] array ) throws IOException {
		int bytesread = 0;
		int i;
		while (bytesread < array.length) {
			i = streamIn.read(array, bytesread, array.length - bytesread);
			if (i < 0) throw new IOException("Ran out of bytes to read!");
			bytesread += i;
		}
	}
	
	/* For testing purpose 
	 *     
		@SuppressWarnings("unused")
		public static void main(String[] args) throws IOException, DataFormatException {
	    	
	    	SgaArchive archiveSGA=getSgaArchive(new File("C:/Program Files/THQ/Dawn of War - Dark Crusade/DXP2/DXP2Data.sga"));
	    	
			SgaArchive archiveSGA2=getSgaArchive(new File("C:/Program Files/THQ/Dawn of War - Dark Crusade/DXP2/DXP2Data.sga"));
	    	
	    	System.out.println(archiveSGA.get_dirName(0));
	    	System.out.println(archiveSGA.get_subDirIDBegin(0));
	    	System.out.println(archiveSGA.get_subDirIDEnd(0));
	    	System.out.println(archiveSGA.get_subFileIDBegin(0));
	    	System.out.println(archiveSGA.get_subFileIDEnd(0));
	    	
	    	System.out.println(archiveSGA.get_fileName(0));
	    	System.out.println(archiveSGA.get_fileName(21));
	    	
	    	System.out.println(archiveSGA.sgaLinkedDirList.get("scar/tutorial"));
	    	System.out.println(archiveSGA.getFilePtr("scar/tutorial/tutorial-ork.scar"));
	    	//archiveSGA.getFile("scar/tutorial2/tutorial-ork.scar");
	    }
	*/
	
}
