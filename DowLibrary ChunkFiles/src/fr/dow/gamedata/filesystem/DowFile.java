package fr.dow.gamedata.filesystem;

import java.io.File;
import java.io.IOException;
import java.util.Vector;

import fr.dow.gamedata.SgaArchive;

public class DowFile {

	private String name;
	private String dowPath;
	
	private Vector<DowFileVersion> versions;

	public DowFile(String dowPath) {
		this.name=dowPath.substring(dowPath.lastIndexOf(DowMod.DOW_PATH_SEPARATOR_CHAR)+1);
		this.dowPath=dowPath;
		versions=new Vector<DowFileVersion>();
	}
	
	public void addDiscFile(File discFile) {
		versions.add(new DowFileVersion(discFile,this));
	}
	
	public void addSgaFile(SgaArchive sgaArchive, Integer sgaPtr, Integer sgaFolderPtr) {
		versions.add(new DowFileVersion(sgaArchive,sgaPtr,sgaFolderPtr,this));
	}
	
	public String getName() {
		return name;
	}
	
	public String getDowPath() {
		return dowPath;
	}
	
	public int getVersionsCount() {
		return versions.size();
	}

	public byte[] getData(int version) throws IOException {
		return versions.get(version).getData();
	}
	
	public DowFileVersion getDowVersion(int version) {
		return versions.get(version);
	}
	
	public DowFileVersion getLastVersion() {
		return versions.lastElement();
	}

	public byte[] getLastVersionData() throws IOException {
		return versions.lastElement().getData();
	}

	public Integer getRank(DowFileVersion dowFileVersion) {
		return versions.indexOf(dowFileVersion);
	}
	
}
