package fr.dow.gamedata.filesystem;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import fr.dow.gamedata.SgaArchive;

public class DowFileVersion {

	private SgaArchive sgaArchive;
	private Integer sgaFilePtr;
	private Integer sgaFolderPtr;
	
	private File file;
	
	private DowFile dowFile;
	
	public DowFileVersion(File file, DowFile dowFile) {
		this.file=file;
		this.dowFile=dowFile;
	}
	
	public DowFileVersion(SgaArchive sgaArchive, Integer sgaPtr, Integer sgaFolderPtr, DowFile dowFile) {
		this.sgaArchive=sgaArchive;
		this.sgaFilePtr=sgaPtr;
		this.sgaFolderPtr=sgaFolderPtr;
		this.dowFile=dowFile;
	}

	public byte[] getData() throws IOException {

		if ((sgaArchive!= null)&(sgaFilePtr!=null)) return sgaArchive.getFile(sgaFilePtr);

		if (file!=null) {
			FileInputStream streamFile = new FileInputStream(file);
			byte[] data=new byte[(int) file.length()];
			streamFile.read(data);
			streamFile.close();
			return data;
		}
		
		throw new FileNotFoundException();
		
	}

	public DowFile getDowFile() {
		return dowFile;
	}
	
	public File getFile() { return file; }

	public Integer getPtr() { return sgaFilePtr; }
	public SgaArchive getSgaArchive() { return sgaArchive; }
	
	public String getGlobalPath() {
		if (file!=null) return file.getAbsolutePath();
		return sgaArchive.get_SgaFile().getAbsolutePath(); // TODO a completer pour un meilleurs affichage
	}

	public DowFileVersion replaceExtension(String oldExt, String newExt) {
		
		if (file!=null) {
			
			String fileName=file.getName();
			
			if (!fileName.endsWith(oldExt)) return null;
			
			String newName=fileName.substring(0,fileName.length()-oldExt.length())+newExt;
			
			File newFile=new File(file.getParent()+DowMod.DOW_PATH_SEPARATOR+newName);
			
			if (!newFile.isFile()) return null;
			
			return new DowFileVersion(newFile,null);
		}
		
		if (sgaArchive!=null) {
			
			String fileName=sgaArchive.get_fileName(sgaFilePtr);
			
			if (!fileName.endsWith(oldExt)) return null;

			String newName=fileName.substring(0,fileName.length()-oldExt.length())+newExt;
			
			Integer newSgaFilePtr=sgaArchive.getFilePtr(newName,sgaFolderPtr);
			
			if (newSgaFilePtr==null) return null;
			
			return new DowFileVersion(sgaArchive, newSgaFilePtr, sgaFolderPtr,null);
			
		}
		
		return null;
		
	}
	
}



