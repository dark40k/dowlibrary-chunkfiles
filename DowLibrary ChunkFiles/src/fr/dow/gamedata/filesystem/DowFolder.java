package fr.dow.gamedata.filesystem;

import java.io.FilenameFilter;
import java.util.HashMap;
import java.util.Vector;

public class DowFolder {

	private String name;
	private String dowPath;
	
	private HashMap<String, DowFolder> folderList; //key is foldername
	private HashMap<String, DowFile> fileList; //key is filename

	public DowFolder(String dowPath) {
		this.name=dowPath.substring(dowPath.lastIndexOf(DowMod.DOW_PATH_SEPARATOR_CHAR)+1);
		this.dowPath=dowPath;
		folderList=new HashMap<String, DowFolder>();
		fileList=new HashMap<String, DowFile>();
	}

	public String getName() {
		return name;
	}
	
	public String getDowPath() {
		return dowPath;
	}
	
	public void addSubDowFolder(DowFolder dowFolder) {
		folderList.put(dowFolder.name, dowFolder);
	}
	
	public void addSubDowFile(DowFile dowFile) {
		fileList.put(dowFile.getName(), dowFile);
	}
	
	public DowFolder getSubDowFolder(String name) {
		return folderList.get(name);
	}
	
	public DowFile getSubDowFile(String name) {
		return fileList.get(name);
	}
	
	public Vector<DowFile> getSubDowFileList(FilenameFilter filter) {
		Vector<DowFile> filterList=new Vector<DowFile>();
		for (DowFile dowFile : fileList.values())
			if (filter.accept(null, dowFile.getName()))
				filterList.add(dowFile);
		return filterList;
	}
	
	public HashMap<String, DowFolder> getSubFoldersList() {
		return folderList;
	}
	
	public HashMap<String, DowFile> getSubFilesList() {
		return fileList;
	}
	
	
	
}
