package fr.dow.gamedata.filesystem;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map.Entry;

import fr.dow.gamedata.IniFile;
import fr.dow.gamedata.SgaArchive;

public class DowMod implements Cloneable {

	public static final Character DOW_PATH_SEPARATOR_CHAR = '/';
	public static final String DOW_PATH_SEPARATOR = DOW_PATH_SEPARATOR_CHAR.toString();

	
	private static final String[] engineMapFoldersOrderedList = {"Data", "%LOCALE%\\Data" }; // Warning : files are listed in loading order list (i.e. reverse to .module)
	private static final String[] engineArchivesOrderedList = {"Engine.sga", "%LOCALE%\\EnginKeys.sga", "%LOCALE%\\EnginLoc.sga" }; // Warning : files are listed in loading order list (i.e. reverse to .module)
	
	private static final String TOPFOLDER_DOWPATH="";

	private static HashMap<File,DowMod>  loadedDowMods=new HashMap<File,DowMod>();

	private final static DowModParams dowModParams = new DowModParams();
	
	private File modFile;
	private File modFolder;

	private DowFolder topFolder;

	private HashMap<String,DowFolder> folderList; // link folder dowPath with folders 
	private HashMap<String,DowFile> fileList; // link file dowPath with files

	//
	// Access method
	// 
	//

	public DowFolder getFolder(String dowPath) {
		return folderList.get(dowPath);
	}

	public DowFile getDowFile(String dowPath) {
		return fileList.get(dowPath);
	}

	public DowFolder getTopFolder() { return topFolder; }

	public byte[] getFileData(String dowPath) throws IOException {
		DowFile dowFile=fileList.get(dowPath);
		if (dowFile==null) return null;
		return dowFile.getLastVersionData();
	}

	public File getModFile() { return modFile;}

	public File getModFolder() {
		return modFolder;
	}

	public String getAbsoluteFilePath(String dowPath) {
		String path = getModFolder().getAbsolutePath().toString()+ "/Data/" + dowPath;
		path=path.replace("/", File.separator);
		return path;
	}
	
	public boolean isValidFile(String dowPath) { return (fileList.get(dowPath)!=null); }
	
	public static DowModParams getDowModParams() {
		return dowModParams;
	}
	
	//
	// Loading & creation functions
	// 

	public static DowMod buildFromModuleFile(File modFile) {
		return buildFromModuleFile(modFile,false);
	}
	
	public static DowMod buildFromModuleFile(File modFile, boolean forceRefresh) {

		if (modFile==null) return null;
		
		if (!modFile.isFile()) return null;

		// check if mod is already stored
		if (loadedDowMods.containsKey(modFile) && !forceRefresh) return loadedDowMods.get(modFile);

		try {

			DowMod dowMod;

			// load mod params
			IniFile modINI=new IniFile(modFile.getAbsolutePath());

			// check if there is a base mod to start from
			String fatherModName=modINI.getStringProperty("global", "RequiredMod.1");
			if (fatherModName!=null) {
				File fatherModFile=new File(modFile.getParent()+File.separator+fatherModName+".module");
				dowMod=(DowMod)buildFromModuleFile(fatherModFile,forceRefresh).clone();
			}
			else dowMod=buildEngineModFromRootFile(modFile.getParentFile());

			System.out.println("Loading Mod file : " + modFile.getAbsolutePath());

			// save mod definition
			dowMod.modFile=modFile;

			//get base folder
			File baseFolder=modFile.getParentFile();
			dowMod.modFolder=new File(baseFolder+File.separator+modINI.getStringProperty("global", "ModFolder"));
			
			// find last SGA archive line in module file
			int searchArchiveNbr=1;
			do {} while (modINI.getStringProperty("global", "ArchiveFile." + searchArchiveNbr++)!=null);
			searchArchiveNbr-=2; // update counter (offset is 2)
			
			// scan SGA archives starting by the last
			while (true) {

				// get base folder name
				String archiveName=modINI.getStringProperty("global", "ArchiveFile." + searchArchiveNbr--);
				if (archiveName==null) break;

				//replace local names (%LOCALE%, %TEXTURE-LEVEL%, ...)
				archiveName=dowModParams.doReplace(archiveName);

				// add SGA content if it exists
				File sgaFile=new File(dowMod.modFolder+File.separator+archiveName+".sga");
				if (sgaFile.isFile()) {
					dowMod.scanSgaFile(sgaFile,dowMod.getTopFolder());
				} else {
					//System.out.println("Warning: archive not found = " + sgaFile.getAbsolutePath() );
				}
			}

			// find last HDD folder to scan
			int searchFolderNbr=1;
			do {} while (modINI.getStringProperty("global", "DataFolder." + searchFolderNbr++)!=null);
			searchFolderNbr-=2; // update counter (offset is 2)
			
			// scan HDD folders starting by the last
			while (true) {

				// get base folder name
				String searchFolderName=modINI.getStringProperty("global", "DataFolder." + searchFolderNbr--);
				if (searchFolderName==null) break;

				//replace local names (%LOCALE%, %TEXTURE-LEVEL%, ...)
				searchFolderName=dowModParams.doReplace(searchFolderName);

				// add folder content if it exists
				File searchFolder=new File(dowMod.modFolder+File.separator+searchFolderName);
				if (searchFolder.isDirectory()) 
					dowMod.scanDiscFolder(searchFolder,dowMod.getTopFolder());

			}
			
			// store mod to avoid reloading
			loadedDowMods.put(modFile, dowMod);

			return dowMod;

		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}

	//
	// Constructor
	//
	
	private DowMod() {
		
		topFolder=new DowFolder(TOPFOLDER_DOWPATH);

		folderList=new HashMap<String,DowFolder>();
		folderList.put(topFolder.getName(), new DowFolder(TOPFOLDER_DOWPATH));

		fileList=new HashMap<String,DowFile>();
	}

	//
	// Clone implementation
	//
	
	@SuppressWarnings("unchecked")
	public Object clone() {
		DowMod o = null;
		try {
			o = (DowMod) super.clone();
		} catch(CloneNotSupportedException e) {
			System.err.println("MyObject can't clone");
		}

		// deep copy is useless as keys and datas are immutables
		o.folderList=(HashMap<String,DowFolder>)o.folderList.clone();
		o.fileList=(HashMap<String,DowFile>)o.fileList.clone();

		return o;
	}

	public static DowMod buildEngineModFromRootFile(File rootFolder) throws IOException {
		
		DowMod dowMod=new DowMod();
		
		File engineFolder = new File(rootFolder+File.separator+"Engine");
		
		System.out.println("Loading Engine : " + engineFolder.getAbsolutePath());
		
		for (String engineArchive : engineArchivesOrderedList ) {
			File sgaFile=new File(engineFolder+File.separator+dowModParams.doReplace(engineArchive));
			if (sgaFile.isFile()) {
				dowMod.scanSgaFile(sgaFile,dowMod.getTopFolder());
			} else {
				//System.out.println("Warning: archive not found = " + sgaFile.getAbsolutePath() );
			}
		}
		
		for (String engineMapFolder : engineMapFoldersOrderedList ) {
			File searchFolder=new File(engineMapFolder+File.separator+dowModParams.doReplace(engineMapFolder));
			if (searchFolder.isDirectory()) 
				dowMod.scanDiscFolder(searchFolder,dowMod.getTopFolder());
		}
		
		return dowMod;
	}
	
	//
	// Utilities
	//
	
	private void scanSgaFile(File sgaFile, DowFolder dowFolder) throws IOException {
		System.out.println("	Scanning Sga file : " + sgaFile);
		
		SgaArchive sgaArchive = SgaArchive.getSgaArchive(sgaFile);
		
//		// calculate fileName
//		String subFolderName=sgaArchive.getRootChunkName();
//
//		// get subFolder in dowFolder
//		DowFolder subDowFolder=dowFolder.getSubDowFolder(subFolderName);
//
//		// create subDowFolder if it does not exist
//		if (subDowFolder==null) {
//
//			// create subDowFolder and store it in dowFolder
//			subDowFolder=new DowFolder(subFolderName);
//			dowFolder.addSubDowFolder(subDowFolder);
//
//			// store reference
//			folderList.put(subFolderName, subDowFolder);
//		}
//
//		// scan folder to add all files and folders 
//		scanSgaFileRec(sgaArchive,sgaArchive.get_topDirID(),subDowFolder);
		scanSgaFileRec(sgaArchive,sgaArchive.get_topDirID(),dowFolder);
	}
	
	/**
	 * Read sga file and create dowFolder architecture
	 * 
	 * @param sgaArchive : soruce archive file to read
	 * @param folderID : folder to scan
	 * @param dowFolder : destination folder
	 */
	private void scanSgaFileRec(SgaArchive sgaArchive, int folderID, DowFolder dowFolder) {
		
		// scan folder for subfolders
		int beginSubFolderId=sgaArchive.get_subDirIDBegin(folderID);
		int endSubFolderId=sgaArchive.get_subDirIDEnd(folderID);
		for (int subFolderID=beginSubFolderId; subFolderID<endSubFolderId; subFolderID++) {

			// calculate fileName
			String subFolderName=sgaArchive.get_dirName(subFolderID);

			// get subFolder in dowFolder
			DowFolder subDowFolder=dowFolder.getSubDowFolder(subFolderName);

			// create subDowFolder if it does not exist
			if (subDowFolder==null) {

				// calculate dowPath
				String subFolderDowPath=sgaArchive.get_dirDowPath(subFolderID);

				// create subDowFolder and store it in dowFolder
				subDowFolder=new DowFolder(subFolderDowPath);
				dowFolder.addSubDowFolder(subDowFolder);

				// store reference
				folderList.put(subFolderDowPath, subDowFolder);
			}

			// scan folder to add all files and folders 
			scanSgaFileRec(sgaArchive,subFolderID,subDowFolder);


		}

		// scan folder for subfiles
		int beginSubFileId=sgaArchive.get_subFileIDBegin(folderID);
		int endSubFileId=sgaArchive.get_subFileIDEnd(folderID);
		for (int subFileID=beginSubFileId; subFileID<endSubFileId; subFileID++) {

			// calculate fileName
			String subFileName=sgaArchive.get_fileName(subFileID);

			// get subDowFile in dowFolder
			DowFile subDowFile=dowFolder.getSubDowFile(subFileName);

			// create subDowFile if it does not exist
			if (subDowFile==null) {

				// calculate dowPath
				String subFileDowPath=dowFolder.getDowPath()+DOW_PATH_SEPARATOR+subFileName;

				// create subDowFile and store it in dowFolder
				subDowFile=new DowFile(subFileDowPath);
				dowFolder.addSubDowFile(subDowFile);

				// store reference
				fileList.put(subFileDowPath, subDowFile);
			}

			// push new version inside subDowFile
			subDowFile.addSgaFile(sgaArchive,subFileID, folderID);

		}

	}

	private void scanDiscFolder(File discFolder, DowFolder dowFolder) {
		System.out.println("	Scanning Game Folder : " + discFolder);
		scanDiscFolderRec(discFolder, dowFolder);
	}
	
	private void scanDiscFolderRec(File discFolder, DowFolder dowFolder) {

		// add all files (folder or files) inside current object which is of folder type
		for (File subFile : discFolder.listFiles()) 

			if (subFile.isDirectory()) {

				// calculate fileName
				String subFolderName=subFile.getName().toLowerCase();

				// get subFolder in dowFolder
				DowFolder subDowFolder=dowFolder.getSubDowFolder(subFolderName);

				// create subDowFolder if it does not exist
				if (subDowFolder==null) {

					// calculate dowPath
					String subFolderDowPath;
					if (dowFolder.getDowPath().length()>0) 
						subFolderDowPath=dowFolder.getDowPath()+DOW_PATH_SEPARATOR+subFolderName;
					else
						subFolderDowPath=subFolderName;

					// create subDowFolder and store it in dowFolder
					subDowFolder=new DowFolder(subFolderDowPath);
					dowFolder.addSubDowFolder(subDowFolder);

					// store reference
					folderList.put(subFolderDowPath, subDowFolder);
				}

				// scan folder to add all files and folders 
				scanDiscFolderRec(subFile,subDowFolder);

			}
			else {

				// calculate fileName
				String subFileName=subFile.getName().toLowerCase();

				// get subDowFile in dowFolder
				DowFile subDowFile=dowFolder.getSubDowFile(subFileName);

				// create subDowFile if it does not exist
				if (subDowFile==null) {

					// calculate dowPath
					String subFileDowPath=dowFolder.getDowPath()+DOW_PATH_SEPARATOR+subFileName;

					// create subDowFile and store it in dowFolder
					subDowFile=new DowFile(subFileDowPath);
					dowFolder.addSubDowFile(subDowFile);

					// store reference
					fileList.put(subFileDowPath, subDowFile);
				}

				// push new version inside subDowFile
				subDowFile.addDiscFile(subFile);
			}
	}


	//
	// Subclass DowModParams for import
	//
	
	public static class DowModParams {

		private static String sectionImportParams = "ImportParams";
		private static String propertyBaseNameSearchKey = "SearchKey.";
		private static String propertyBaseNameReplaceString = "ReplaceString.";
		
		private final HashMap<String,String> replaceList = new HashMap<String,String>();
		
		//
		// Creation
		//
		
		public DowModParams() {
			resetDefault();
		}
		
		public void resetDefault() {
			replaceList.clear();
			replaceList.put("%LOCALE%", "Locale/English");
			replaceList.put("%MODEL-LEVEL%", "High");
			replaceList.put("%TEXTURE-LEVEL%", "Full");
			replaceList.put("%SOUND-LEVEL%", "Full");
			
			// parameters have changed => loaded mods are no longer valid
			loadedDowMods.clear();
		}
		
		//
		// Execute string replacement
		//
		
		public String doReplace(String stringIn) {
			for (Entry<String,String> entry : replaceList.entrySet() ) 
				stringIn = stringIn.replace(entry.getKey(), entry.getValue());
			return stringIn;
		}
		
		//
		// Import / Export functions
		//
		
		public void importFromIniFile(IniFile iniFile) {
			
			if (!iniFile.containsSection(sectionImportParams)) {
				resetDefault();
				return;
			}
			
			replaceList.clear();
			
			Integer lineNbr = 0;
			do {
				String searchKey=iniFile.getStringProperty(sectionImportParams, propertyBaseNameSearchKey+lineNbr);
				String replaceString=iniFile.getStringProperty(sectionImportParams, propertyBaseNameReplaceString+lineNbr);
				
				if ((searchKey!=null) && (replaceString!=null))
					replaceList.put(searchKey,replaceString);
				else
					break;
				
				lineNbr++;
				
			} while (true);

			// parameters have changed => loaded mods are no longer valid
			loadedDowMods.clear();
		}

		public void exportToIniFile(IniFile iniFile) {
			
			iniFile.addSection(sectionImportParams, null);
			
			Integer lineNbr = 0;
			for (Entry<String,String> entry : replaceList.entrySet() ) {
				iniFile.setStringProperty(
						sectionImportParams, 
						propertyBaseNameSearchKey+lineNbr, 
						entry.getKey(), 
						null);			
				iniFile.setStringProperty(
						sectionImportParams, 
						propertyBaseNameReplaceString+lineNbr, 
						entry.getValue(), 
						null);	
				lineNbr++;
			}
					
		}
		
		public void clear() {
			replaceList.clear();
			loadedDowMods.clear();
		}
		
		public void addSearchReplaceKey(String searchKey, String replaceString) {
			replaceList.put(searchKey,replaceString);
			loadedDowMods.clear();
		}
		
		@SuppressWarnings("unchecked")
		public HashMap<String,String> getSearchReplaceKeys() {
			return (HashMap<String,String>) replaceList.clone();
		}
		
	}
}
